<?php
	/**
	 * string	$args['rows']
	 * string	$args['rows'][0]['image']
	 * string	$args['rows'][0]['title']
	 * string	$args['rows'][0]['text']
	 * string	$args['rows'][0]['button_text']
	 * string	$args['rows'][0]['button_link']
	 */
	global $args;
?>
<?php if(is_array($args['rows'])): $row_counter = 0; ?>
	<div class='team-section'>
		<div class="interior-box">
			<h3>OUR TEAM</h3>
		</div>
		<div class='team-content-section interior-box'>
	<?php foreach( $args['rows'] as $row ) : ?>
					<div class='member'>
							<img class='team-block-image' src='<?php echo $row['image']['url']; ?>'>
							<div class="member-content">
								<h4 class="member-title"><?php echo $row['title']; ?></h4>
								<h5><?php echo $row['text']; ?></h5>
								<a target="_blank" href="<?php echo $row['button_text']; ?>"><img class="in" src="/wp-content/themes/pilot/mason-modules/team/img/Social-LinkedIn.svg"></a>
							</div>
					</div><!--team-content-section--->
	<?php endforeach; ?>
  		</div><!--/team-section--->
	</div>
<?php endif; ?>