<?php 
	/**
	 * string	$args['title']
	 * string	$args['content']
	 */
	global $args;
?>
<div class="para-wrapper">
	<img class="image-one" id="image_one" src="<?php echo $args['image_one']['url']; ?>">
	<img class="image-two" id="image_two" src="<?php echo $args['image_two']['url']; ?>">
</div>
		<script>
    var imageTwo = document.querySelector("#image_two");
    function setTranslate(xPos, yPos, el) {
        el.style.transform = "translate3d(" + xPos + "px, " + yPos + "px, 0)";
    } 
    window.addEventListener("DOMContentLoaded", scrollLoop, false);

    var xScrollPosition;
    var yScrollPosition;
 
    function scrollLoop() {
		$bg_image = $('.image-one');
        var image_top = $bg_image.offset().top - $bg_image.position().top,
			yScrollPosition = window.scrollY - (image_top ) - 00,
			denom = $(document).height() - $(window).height(),
			coeff = window.scrollY/denom,
			xScrollPosition = 0;//-.1*($bg_image.width()) + .2 * coeff*($bg_image.width());
        setTranslate(xScrollPosition, yScrollPosition * -.3, imageTwo); 
        requestAnimationFrame(scrollLoop);
    }

$( document ).ready(function() {
	function paraHeight(){
		var image_height = $('#image_one').height();
//		$('.block-diffParallax,.para-wrapper').height( (image_height*1.5) + 'px');
	}
	paraHeight();
	$(window).on('resize', function(){
		paraHeight();
	});
});
</script>
<style type="text/css">
	.para-wrapper{
		//max-width:1200px;
		//height:740px;
		display:inline-block;
		width:100%;
		position:relative;
		margin:60px 0;
	}
	.block-diffParallax{
		position:relative;
		background-color:#202020;
//height:900px;
	padding-top:60px;
	padding-bottom:60px;
		text-align:center;
	}

	.block-diffParallax .layout-content{
		position:relative;
		text-align: center;
	}
	.image-one{
		position:absolute;
		bottom:0;
		left:0;
		z-index:3;
		height:auto;
		width:80%;
		//height:auto;
//		background-image:url('<?php echo $args['image_one']['url']; ?>');
	}
	.image-two{
		z-index:0;
		float:right;
		//position:absolute;
		right:0;
		bottom:-40px;
		height:auto;
		//height:auto;
		width:80%;
		background-color:transparent;
	}

</style>