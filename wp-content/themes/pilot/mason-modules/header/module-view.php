<?php 
	/**
	 * string	$args['title']
	 * string	$args['content']
	 */
	global $args;
	global $wp;
	$current_url = home_url( add_query_arg( array(), $wp->request ) );
?>
<div class="header">
	<div class="orange-fade-horizontal-reverse"></div>
	<div class="content ">
		<a href="/"><img class="logo" src="<?php echo $args['logo']['url']; ?>"></a>
		<div class="main-nav">
			<?php if( count($args['main_links'])>0): ?>
				<ul class="primary-nav">
					<?php foreach($args['main_links'] as $main): 
						$main_href = $main['header_block_main_link']['url'];
						$active_class = "";
						if( rtrim($current_url,'/') == rtrim($main_href,'/') ){
							$active_class = " active";
						}
					?>
						<?php $subs = ""; if( is_array($main['header_block_sublinks']) && count($main['header_block_sublinks'])>0): ?>
							<?php foreach($main['header_block_sublinks'] as $sub ) : 
								if( isset($sub['header_block_sub_link']['url'])){
									$sub_href = $sub['header_block_sub_link']['url'];
									if( rtrim($current_url,'/') == rtrim($sub_href,'/') ){
										$active_class = " active";
									}
									$subs .= '<li><a href="'.$sub_href.'">'.$sub['header_block_sub_link']['title'].'</a></li>';
								}
							endforeach; ?>
						<?php endif; ?>
						<li class=""><a data-anchor="<?php echo $main['header_block_main_link']['anchor']; ?>" href="#<?php echo $main['header_block_main_link']['anchor']; ?>">
								<?php echo $main['header_block_main_link']['title']; ?>
							</a>
							<?php if($subs) : ?>
								<ul class="sub-nav">
									<?php echo $subs; ?>
								</ul>
							<?php endif; ?>
						</li>
					<?php endforeach; ?>
				</ul>
			<?php endif; ?>
	<div class="mobile-nav">
		<div class="menu-button">
			<img class="menu-open" src="<?php echo get_template_directory_uri(); ?>/mason-modules/header/img/Icon-Plus.svg">
			<img class="menu-close" src="<?php echo get_template_directory_uri(); ?>/mason-modules/header/img/Icon-Minus.svg">
		</div>

	</div>

		</div>
<!--
		<div class="links">
			<a href='/'>BACK TO HOMEPAGE</a>
		</div>
	-->

	</div>
	<div class="mobile-menu">
		<?php if( count($args['main_links'])>0): ?>
			<ul class="primary-nav">
				<?php foreach($args['main_links'] as $main): 
					$main_href = $main['header_block_main_link']['url'];
					$active_class = "";
					if( rtrim($current_url,'/') == rtrim($main_href,'/') ){
						$active_class = " active";
					}
				?>
					<?php $subs = ""; if( is_array($main['header_block_sublinks']) && count($main['header_block_sublinks'])>0): ?>
						<?php foreach($main['header_block_sublinks'] as $sub ) : 
							if( isset($sub['header_block_sub_link']['url'])){
								$sub_href = $sub['header_block_sub_link']['url'];
								if( rtrim($current_url,'/') == rtrim($sub_href,'/') ){
									$active_class = " active";
								}
								$subs .= '<li><a href="'.$sub_href.'">'.$sub['header_block_sub_link']['title'].'</a></li>';
							}
						endforeach; ?>
					<?php endif; ?>
					<li class="<?php echo $active_class; ?>"><a data-anchor="<?php echo $main['header_block_main_link']['anchor']; ?>"  href="<?php echo $main_href; ?>">
							<?php echo $main['header_block_main_link']['title']; ?>
						</a>
						<?php if($subs) : ?>
							<ul class="sub-nav">
								<?php echo $subs; ?>
							</ul>
						<?php endif; ?>
					</li>
				<?php endforeach; ?>
			</ul>
		<?php endif; ?>
		<div id="footer-menus">
		</div>
	</div>
</div>
<script>
	$('.menu-button').click(function(){
		if( $('.block-header').hasClass('open-mobile') ){
			$('.block-header').removeClass('open-mobile');
		}
		else{
			$('.block-header').addClass('open-mobile');
		}
	});
	$( document ).ready(function() {
		$(".column-two").clone().appendTo( $('#footer-menus'));
		$(".column-three").clone().appendTo( $('#footer-menus'));

		$("a[data-anchor]").click(function(e){
			e.preventDefault();
			var target = $(e.target).data('anchor');
			$('.block-header').removeClass('open-mobile');
			$([document.documentElement, document.body]).animate({
		        scrollTop: $("#" + target).offset().top
		    }, 1500);
		});
	});
</script>