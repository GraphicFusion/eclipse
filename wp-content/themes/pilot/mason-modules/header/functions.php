<?php
	function build_header_layout(){
		$args = array(
			'title' => get_sub_field('header_block_title'),
			'content' => mason_get_sub_field('header_block_content'),
			'logo' => mason_get_sub_field('header_block_logo'),
			'main_links' => mason_get_sub_field('header_block_main_nav'),
		);
		$args['main_links'] = [
			[
			 'header_block_main_link' => [
                            'title' => 'Features',
                            'url' => 'http://eclipse.test/',
                            'target' => '',
                            'anchor' => 'features_anchor'
                        ],
            ],
            [
			 'header_block_main_link' => [
                            'title' => 'Team',
                            'url' => 'http://eclipse.test/',
                            'target' => '',
                            'anchor' => 'team_block_4'                            
                        ],
            ],
            [
			 'header_block_main_link' => [
                            'title' => 'Get Updates',
                            'url' => 'http://eclipse.test/',
                            'target' => '',
                            'anchor' => 'footer-content'
                        ]
            ]           

		];
		return $args;
	}
?>