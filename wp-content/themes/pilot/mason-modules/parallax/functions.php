<?php
	function build_parallax_layout(){
		$args = array(
			'image_one' => mason_get_sub_field('parallax_block_image_one'),
			'image_two' => mason_get_sub_field('parallax_block_image_two'),
		);
		return $args;
	}
?>