<?php
	global $pilot;
	// add module layout to flexible content
	$name = "parallax"; 
	$module_layout = array (
		'key' => '569d9ff728231',
		'name' => $name . '_block',
		'label' => 'Parallax',
		'display' => 'block',
		'sub_fields' => array (
	        array(
	            'key' => create_key($name,'image_one'),
	            'label' => 'First Image',
	            'name' => $name.'_block_image_one',
	            'type' => 'image',
	            'instructions' => '',
	            'required' => 0,
	            'conditional_logic' => array(),
	            'wrapper' => array(
	                'width' => '100%',
	                'class' => '',
	                'id' => '',
	            ),
	            'return_format' => 'array',
	            'preview_size' => 'thumbnail',
	            'library' => 'all',
	            'min_width' => '',
	            'min_height' => '',
	            'min_size' => '',
	            'max_width' => '',
	            'max_height' => '',
	            'max_size' => '',
	            'mime_types' => '',
	        ),
	        array(
	            'key' => create_key($name,'image_two'),
	            'label' => 'Image',
	            'name' => $name. '_block_image_two',
	            'type' => 'image',
	            'instructions' => '',
	            'required' => 0,
	            'conditional_logic' => array(),
	            'wrapper' => array(
	                'width' => '100%',
	                'class' => '',
	                'id' => '',
	            ),
	            'return_format' => 'array',
	            'preview_size' => 'thumbnail',
	            'library' => 'all',
	            'min_width' => '',
	            'min_height' => '',
	            'min_size' => '',
	            'max_width' => '',
	            'max_height' => '',
	            'max_size' => '',
	            'mime_types' => '',
	        ),
		),
		'min' => '',
		'max' => '',
	);
?>