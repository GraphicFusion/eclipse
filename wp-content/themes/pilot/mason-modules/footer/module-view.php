<?php 
	/**
	 * string	$args['title']
	 * string	$args['content']
	 */
	global $args;
?>
<div class="footer">
	<div class="footer-content interior-container interior-box">
		<div class="logo-wrapper">
			<img src="<?php echo $args['logo']['url']; ?>">
		</div>
		<div class="column-wrapper">
<a target="_blank" href="https://www.facebook.com/eclipseminingtechnologies/">
	<img class="fb" src="/wp-content/themes/pilot/mason-modules/footer/img/Icon-Facebook.svg">
</a>
<a target="_blank" href="https://www.linkedin.com/company/eclipsemining/">
	<img class="in" src="/wp-content/themes/pilot/mason-modules/footer/img/Icon-LinkedIn.svg">
</a>
			<?php $cols = ['one']; foreach($cols as $col) : ?>
					<?php if( is_array($args['col_'.$col] ) && count($args['col_'.$col])>0 ) : ?>
						<?php foreach($args['col_'.$col] as $link_arr ) : $link = $link_arr['footer_col_'.$col.'_link']; ?>
								<?php echo $link; ?>
						<?php endforeach; ?>
					<?php endif; ?>
			<?php endforeach; ?>
		</div>

	</div>
</div>