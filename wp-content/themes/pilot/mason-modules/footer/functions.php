<?php
	function build_footer_layout(){
		$args = array(
			'pull_down' => mason_get_sub_field('footer_block_pull_down'),
			'title' => mason_get_sub_field('footer_block_title'),
			'logo' => mason_get_sub_field('footer_block_logo'),
			'col_one' => mason_get_sub_field('footer_column_one'),
			'col_two' => mason_get_sub_field('footer_column_two'),
			'col_three' => mason_get_sub_field('footer_column_three')			
		);
		return $args;
	}
?>