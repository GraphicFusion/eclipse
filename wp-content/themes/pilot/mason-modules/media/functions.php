<?php
	function build_media_layout(){
		global $i;
		$half_width = mason_get_sub_field('media_block_two_half_widths');
		$suffixes = [""];
		if($half_width){
			$suffixes[] = "_second";
		}
		$args = [];
		foreach($suffixes as $suffix){
			$media_args = array(
				'half_width'.$suffix => $half_width,
				'subtitle'.$suffix => mason_get_sub_field('media_block_subtitle'.$suffix),
				'title'.$suffix => mason_get_sub_field('media_block_title'.$suffix),
				'button'.$suffix => mason_get_sub_field('media_block_link'.$suffix),
				'id'.$suffix => 'media_block'.$suffix.'_'.$i,
				'overlay_color'.$suffix => '',
				'overlay_opacity'.$suffix => '',
				'left_align_title'.$suffix => mason_get_sub_field('left_align_title'.$suffix),
				'content'.$suffix => mason_get_sub_field('media_block_content'.$suffix),
				'show_content'.$suffix => mason_get_sub_field('media_block_use_content'.$suffix),
				'use_banner'.$suffix => mason_get_sub_field('media_block_use_banner'.$suffix),
				'use_image'.$suffix => mason_get_sub_field('media_block_use_bg_image'.$suffix),
				'use_popup'.$suffix => mason_get_sub_field('media_block_use_popup'.$suffix),
				'use_overlay'.$suffix => mason_get_sub_field('media_block_modify'.$suffix)
			);
			if( mason_get_sub_field('media_block_modify'.$suffix) ){
				if( $opacity = mason_get_sub_field('media_block_overlay_opacity'.$suffix) ){
					$media_args['overlay_opacity'.$suffix] = $opacity;
				}

				if( $percent = mason_get_sub_field('media_block_overlay_percent'.$suffix) ){
					$media_args['percent'.$suffix] = $percent;
				}
				else{
					$media_args['percent'.$suffix] = "50";				
				}
				if( $color = mason_get_sub_field('media_block_overlay_color'.$suffix) ){
					$media_args['overlay_color'.$suffix] = $color;
				}
			}		
			$image = mason_get_sub_field('media_block_image'.$suffix);
			if( is_array( $image ) ){
				$media_args['bg_image_url'.$suffix] = $image['url'];
			}
			$mp4_file = mason_get_sub_field('media_video_file_mp4'.$suffix);
			if( is_array( $mp4_file ) ){
				//$media_args['width_class'.$suffix] . " video-media";
				$media_args['video_file_mp4'.$suffix] = $mp4_file['url'];
				$media_args['fallback_image_url'.$suffix] = $image['url'];
			}
			$media_args['youtube'.$suffix] = mason_get_sub_field('media_block_youtube'.$suffix);
			$bg_mp4_file = mason_get_sub_field('media_bg_video_file_mp4'.$suffix);
			if( is_array( $bg_mp4_file ) ){
				$media_args['bg_video_file_mp4'.$suffix] = $bg_mp4_file['url'];
			}
			$media_args['bg_youtube'.$suffix] = mason_get_sub_field('media_block_bg_youtube'.$suffix);
			$args = array_merge($media_args,$args);
		}
		if( is_array( $args ) ){
			return $args;
		}
	}
	function media_admin_enqueue($hook) {
		wp_register_style( 'media_wp_admin_css', get_template_directory_uri() . '/mason-modules/media/admin-style.css', false, '1.0.0' );
        wp_enqueue_style( 'media_wp_admin_css' );
    }
	add_action( 'admin_enqueue_scripts', 'media_admin_enqueue' );
?>