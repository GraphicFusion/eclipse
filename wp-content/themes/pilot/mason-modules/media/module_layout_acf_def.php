<?php
global $pilot;
// add module layout to flexible content
$use_halfwidth = 0;
function build_array($suffix = "", $name = "", $class = ""){
    $conditional_logic =  array(
            'field' => create_key('media','two_half_widths'),
            'operator' => '==',
            'value' => '0',
        );
    if('second' == $class){
        $conditional_logic = array(
            'field' => create_key('media','two_half_widths'),
            'operator' => '==',
            'value' => '1',
        );
    }
    $subfields = [
        array(
            'key' => create_key('media','use_popup'.$suffix),
            'label' => 'Add a Popup Video' . $name,
            'name' => 'media_block_use_popup'.$suffix,
            'type' => 'true_false',
            'instructions' => '',
            'required' => 0,
            'conditional_logic' => array(array($conditional_logic)),
            'wrapper' => array(
                'width' => '25',
                'class' => $class,
                'id' => '',
                'height' => '200px'
            ),
            'message' => '',
            'default_value' => 0,
            'ui' => 1,
            'ui_on_text' => 'Popup',
            'ui_off_text' => 'No Popup',
        ),

        array(
            'key' => create_key('media','video_file_mp4'.$suffix),
            'label' => 'Popup Video MP4' . $name,
            'name' => 'media_video_file_mp4'.$suffix,
            'type' => 'file',
            'instructions' => 'Choose a MP4 video with a resolution of at least 1280x720',
            'required' => 0,
            'conditional_logic' => array(
                array(
                    array(
                        'field' => create_key('media','use_popup'.$suffix),
                        'operator' => '==',
                        'value' => '1',
                    ),
                    $conditional_logic
                ),
            ),
            'wrapper' => array(
                'width' => '35%',
                'class' => $class,
                'id' => '',
            ),
            'return_format' => 'array',
            'library' => 'all',
            'min_size' => '',
            'max_size' => '',
            'mime_types' => 'mp4',
        ),
        array(
            'key' => create_key('media','youtube'.$suffix),
            'label' => 'Popup Link (Vimeo or YouTube) ' . $name,
            'name' => 'media_block_youtube'.$suffix,
            'type' => 'text',
            'instructions' => 'This will override any MP4 attached',
            'required' => 0,
            'conditional_logic' => array(
                array(
                    array(
                        'field' => create_key('media','use_popup'.$suffix),
                        'operator' => '==',
                        'value' => '1',
                    ),
                    $conditional_logic
                ),
            ),
            'wrapper' => array(
                'width' => '35',
                'class' => $class,
                'id' => '',
            ),
            'default_value' => '',
            'placeholder' => '',
            'prepend' => '',
            'append' => '',
            'maxlength' => '',
            'readonly' => 0,
            'disabled' => 0,
        ),

        array(
            'key' => create_key('media','space'.$suffix),
            'label' => $name,
            'name' => 'media_block_space'.$suffix,
            'type' => 'message',
			'instructions' => '',
			'required' => 0,
			'conditional_logic' => 0,
			'wrapper' => array(
				'width' => '100',
				'class' => '',
				'id' => '',
			),
			'message' => '',
			'new_lines' => 'wpautop',
			'esc_html' => 0,
        ),
        array(
            'key' => create_key('media','use_bg_image'.$suffix),
            'label' => 'Banner Image or Video' . $name,
            'name' => 'media_block_use_bg_image'.$suffix,
            'type' => 'true_false',
            'instructions' => '',
            'required' => 0,
            'wrapper' => array(
                'width' => '20%',
                'class' => $class,
                'id' => '',
            ),
            'ui' => 1,
            'ui_on_text' => 'Image',
            'ui_off_text' => 'Video',
        ),
        array(
            'key' => create_key('media','use_parallax'.$suffix),
            'label' => 'Use Parallax' . $name,
            'name' => 'media_block_use_parallax'.$suffix,
            'type' => 'true_false',
            'instructions' => '',
            'required' => 0,
            'conditional_logic' => [[$conditional_logic]],
            'wrapper' => array(
                'width' => '20',
                'class' => $class,
                'id' => '',
            ),
            'message' => '',
            'default_value' => 0,
            'ui' => 1,
            'ui_on_text' => 'Use',
            'ui_off_text' => 'Dont Use',
        ),

        array(
            'key' => create_key('media','modify'.$suffix),
            'label' => 'Apply Filter Over Background Image' . $name,
            'name' => 'media_block_modify'.$suffix,
            'type' => 'true_false',
            'instructions' => '',
            'required' => 0,
            'wrapper' => array(
                'width' => '20',
                'class' => $class,
                'id' => '',
            ),
            'message' => '',
            'default_value' => 0,
            'ui' => 1,
            'ui_on_text' => 'Overlay',
            'ui_off_text' => 'No Overlay',            
        ),
        array(
            'key' => create_key('media','bg_video_file_mp4'.$suffix),
            'label' => 'Background Video MP4' . $name,
            'name' => 'media_bg_video_file_mp4'.$suffix,
            'type' => 'file',
            'instructions' => 'Choose a MP4 video with a resolution of at least 1280x720',
            'required' => 0,
            'conditional_logic' => array(
                array(
                    array(
                        'field' => create_key('media','use_bg_image'.$suffix),
                        'operator' => '==',
                        'value' => '0',
                    ),
                    $conditional_logic                    
                ),
            ),
            'wrapper' => array(
                'width' => '30%',
                'class' => $class,
                'id' => '',
            ),
            'return_format' => 'array',
            'library' => 'all',
            'min_size' => '',
            'max_size' => '',
            'mime_types' => 'mp4',
        ),
        array(
            'key' => create_key('media','bg_youtube'.$suffix),
            'label' => 'Background Vimeo Link' . $name,
            'name' => 'media_block_bg_youtube'.$suffix,
            'type' => 'text',
            'instructions' => 'This will override any MP4 attached',
            'required' => 0,
            'conditional_logic' => array(
                array(
                    array(
                        'field' => create_key('media','use_bg_image'.$suffix),
                        'operator' => '==',
                        'value' => '0',
                    ),
                    $conditional_logic
                ),
            ),
            'wrapper' => array(
                'width' => '30',
                'class' => $class,
                'id' => '',
            ),
            'default_value' => '',
            'placeholder' => '',
            'prepend' => '',
            'append' => '',
            'maxlength' => '',
            'readonly' => 0,
            'disabled' => 0,
        ),        
        array(
            'key' => create_key('media','image'.$suffix),
            'label' => 'Background Image' . $name,
            'name' => 'media_block_image'.$suffix,
            'type' => 'image',
            'instructions' => '',
            'required' => 0,
            'conditional_logic' => array(
                array(
                    array(
                        'field' => create_key('media','use_bg_image'.$suffix),
                        'operator' => '==',
                        'value' => '1',
                    ),
                    $conditional_logic                    
                ),
            ),
            'wrapper' => array(
                'width' => '30%',
                'class' => $class,
                'id' => '',
            ),
            'return_format' => 'array',
            'preview_size' => 'thumbnail',
            'library' => 'all',
            'min_width' => '',
            'min_height' => '',
            'min_size' => '',
            'max_width' => '',
            'max_height' => '',
            'max_size' => '',
            'mime_types' => '',
        ),
        array(
            'key' => create_key('media','overlay_opacity'.$suffix),
            'label' => 'Opacity' . $name,
            'name' => 'media_block_overlay_opacity'.$suffix,
            'type' => 'number',
            'instructions' => 'Set from 0 to 1 (for example 0.75)',
            'required' => 0,
            'conditional_logic' => array(
                array(
                    array(
                        'field' => create_key('media','modify'.$suffix),
                        'operator' => '==',
                        'value' => '1',
                    ),
                    $conditional_logic
                ),
            ),
            'wrapper' => array(
                'width' => 35,
                'class' => $class,
                'id' => '',
            ),
            'default_value' => '',
            'placeholder' => '',
            'prepend' => '',
            'append' => '',
            'min' => 0,
            'max' => 1,
            'step' => '.01',
            'readonly' => 0,
            'disabled' => 0,
        ),
        array(
            'key' => create_key('media','overlay_percent'.$suffix),
            'label' => 'Percentage of "cover"' . $name,
            'name' => 'media_block_overlay_percent'.$suffix,
            'type' => 'number',
            'instructions' => 'Set from 0 to 100',
            'required' => 0,
            'conditional_logic' => array(
                array(
                    array(
                        'field' => create_key('media','modify'.$suffix),
                        'operator' => '==',
                        'value' => '1',
                    ),
                    $conditional_logic                    
                ),
            ),
            'wrapper' => array(
                'width' => 30,
                'class' => $class,
                'id' => '',
            ),
            'default_value' => '',
            'placeholder' => '',
            'prepend' => '',
            'append' => '',
            'min' => 0,
            'max' => 100,
            'step' => '.01',
            'readonly' => 0,
            'disabled' => 0,
        ),
        array(
            'key' => create_key('media','overlay_color'.$suffix),
            'label' => 'Color' . $name,
            'name' => 'media_block_overlay_color'.$suffix,
            'type' => 'color_picker',
            'instructions' => 'Set the color overlay for the image or video.',
            'required' => 0,
            'conditional_logic' => array(
                array(
                    array(
                        'field' => create_key('media','modify'.$suffix),
                        'operator' => '==',
                        'value' => '1',
                    ),
                    $conditional_logic                    
                ),
            ),
            'wrapper' => array(
                'width' => '35',
                'class' => $class,
                'id' => '',
            ),
            'default_value' => '',
        ),
/*
        array(
            'key' => create_key('media','subtitle'.$suffix),
            'label' => 'Subtitle' . $name,
            'name' => 'media_block_subtitle'.$suffix,
            'type' => 'text',
            'instructions' => '',
            'required' => 0,
            'conditional_logic' => array(
                array(
                    array(
                        'field' => create_key('media','use_content'.$suffix),
                        'operator' => '==',
                        'value' => '1',
                    ),
                    $conditional_logic
                ),
            ),
            'wrapper' => array(
                'width' => '',
                'class' => $class,
                'id' => '',
            ),
            'default_value' => '',
            'placeholder' => '',
            'prepend' => '',
            'append' => '',
            'maxlength' => '',
            'readonly' => 0,
            'disabled' => 0,
        ),
        */
		array(
            'key' => create_key('media','title'.$suffix),
            'label' => 'Title' . $name,
            'name' => 'media_block_title'.$suffix,
            'type' => 'text',
            'instructions' => '',
            'required' => 0,
            'wrapper' => array(
                'width' => 100,
                'class' => $class,
                'id' => '',
            ),
            'default_value' => '',
            'placeholder' => '',
            'prepend' => '',
            'append' => '',
            'maxlength' => '',
            'readonly' => 0,
            'disabled' => 0,
        ),
        array (
            'key' => create_key('media','content'.$suffix),
            'label' => 'Content' . $name,
            'name' => 'media_block_content'.$suffix,
            'type' => 'wysiwyg',
            'instructions' => '',
            'required' => 0,
            'wrapper' => array (
                'width' => '',
                'class' => $class,
                'id' => '',
            ),
            'default_value' => '',
            'tabs' => 'all',
            'toolbar' => 'full',
            'media_upload' => 1,
        ),
        /*
        array(
            'key' => create_key('media','link'.$suffix),
            'label' => 'Button Link' . $name,
            'name' => 'media_block_link'.$suffix,
            'type' => 'link',
            'instructions' => '',
            'required' => 0,
            'wrapper' => array(
                'width' => '100',
                'class' => $class,
                'id' => '',
            ),
            'post_type' => '',
            'taxonomy' => '',
            'allow_null' => 0,
            'multiple' => 0,
            'return_format' => 'object',
            'ui' => 1,
        ),
        */
    ];
    return $subfields;
}
$subfields_first = build_array();
$subfields_second = [];
if( $use_halfwidth == 1){
	$subfields_second = build_array('_second',' - Second Card', 'second');
        array_unshift(
            $subfields,
            array(
                'key' => create_key('media','two_half_widths'),
                'label' => 'Block Width - One Full or Two Halves ',
                'name' => 'media_block_two_half_widths',
                'type' => 'true_false',
                'instructions' => '',
                'required' => 0,
                'conditional_logic' => 0,
                'wrapper' => array(
                    'width' => '100',
                    'class' => '',
                    'id' => '',
                ),
                'message' => '',
                'default_value' => 0,
                'ui' => 1,
                'ui_on_text' => 'Half Width',
                'ui_off_text' => 'Full Width',
            )
        );
}
$subfields = array_merge($subfields_first, $subfields_second);
$module_layout = array(
    'key' => create_key('media','block'),
    'name' => 'media_block',
    'label' => 'Media Block',
    'display' => 'block',
    'sub_fields' => $subfields,
    'min' => '',
    'max' => '',
);

?>