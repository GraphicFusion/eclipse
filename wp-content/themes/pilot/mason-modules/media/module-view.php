<?php
	global $args;
	$block_class = "";
	$suffixes = [""];
	if($args['half_width'] ){
		$block_class = "half-width";
		$suffixes[] = "_second";
	}
?>
<div class="interior-container media-wrap" >
<?php
	foreach($suffixes as $suffix){
		$use_popup = 0;
		if( $args['use_popup'.$suffix] ){
			$use_popup = 1;
			$popup_video = $args['video_file_mp4'.$suffix];
			if( $args['youtube'.$suffix] ) {
				$popup_video = $args['youtube'.$suffix];
			}
		}
		$use_banner = 0;
		if( $args['use_banner'.$suffix] && (@$args['video_file_mp4'.$suffix] || $args['youtube'.$suffix] || $args['bg_image_url'.$suffix] ) ){
			$use_banner = 1;
		}
		$use_bg_image = 0;
		$use_bg_video = 1;
		if($args['use_image'.$suffix]){
			$use_bg_image = 1;
			$use_bg_video = 0;		
		}
		$use_overlay = 0;
		if( $args['use_overlay'.$suffix] ){
			$use_overlay = 1;
		}
	?>
		<div class="shadow-card <?php echo $block_class." ".$suffix; ?>">							
				<div class="img-block-wrap" id="<?php echo $args['id'.$suffix]; ?>">
					<div class="bg-image" 
					<?php 
//if( $use_bg_image){ echo 'style="background-image: url('.$args['bg_image_url'.$suffix].');'; } ?>
					">


					<img class="stuck-image" style=" height:auto; width:100%;z-index:0; position:relative;" src="<?php if( $use_bg_image){ echo $args['bg_image_url'.$suffix]; } ?>">

						<?php if( $use_overlay ) : ?>
							<div class="img-overlay" style="background-image:linear-gradient(to top right, <?php echo $args['overlay_color'.$suffix]; ?> <?php echo $args['percent'.$suffix]; ?>%, transparent ); opacity: <?php echo $args['overlay_opacity'.$suffix]; ?>;">
							</div>
						<?php endif; ?>
					<div class="media-content" style="z-index:3;">
					<div class="title-div">
						<div class="title-wrap">
<?php $use_title = 1; ?>
							<?php if( $use_title && isset( $args['title'.$suffix] ) ) : ?>
								<h2><?php echo $args['title'.$suffix]; ?></h2>
							<?php endif; ?>
							<?php if(  $args['content'.$suffix]  ) : ?>
								<?php echo $args['content'.$suffix]; ?>
							<?php endif; ?>
						<?php if( $use_popup ) : ?>
							<a class="video-btn html5lightbox" href="<?php echo $popup_video; ?>">
								<img class="in" src="/wp-content/themes/pilot/mason-modules/media/img/Icon-Video.svg">
							</a>
							<a class="video-btn html5lightbox" href="<?php echo $popup_video; ?>">							
								<span>WATCH VIDEO</span>
								<!--
								<svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" viewBox="0 0 100 100">
								  <defs><style>.cls-1 {clip-path: url(#clip-Icon-Video);}.cls-2 {fill: red;}.cls-3 {opacity:1;fill: #fff;}</style>
								    <clipPath id="clip-Icon-Video"><rect width="100" height="100"/></clipPath>
								  </defs><g id="Icon-Video" class="cls-1"><circle id="Ellipse_2" data-name="Ellipse 2" class="cls-2" cx="50" cy="50" r="50"/><g id="Group_170" data-name="Group 170" transform="translate(34 26.001)"><path id="Path_133" data-name="Path 133" class="cls-3" d="M38.784,21.944,4.536.728C2.04-.9,0,.3,0,3.408V44.592c0,3.1,2.04,4.312,4.536,2.68L38.784,26.056A2.926,2.926,0,0,0,40,24,2.926,2.926,0,0,0,38.784,21.944Z" transform="translate(0 0)"/></g></g>
								</svg>
							-->
							</a>
						<?php endif; ?>
							<br>
							<a data-anchor="features_block_1" href="#features_block_1"><img  data-anchor="features_block_1" src="/wp-content/themes/pilot/mason-modules/media/img/Icon-Down.svg"></a>
						</div><!--/title-wrap-->
					</div><!--/title-->

						<?php if( $use_bg_video) : ?>
							<div class="video" data-vide-bg="mp4: <?php echo $args['video_file_mp4'.$suffix]; ?>"></div>
						<?php endif; ?>
										</div><!--/meadi-foot-content-->

					</div><!--/bg-img-->
				</div><!--img-block-wrap-->
		</div>
	<?php } ?>
</div>
<script>
	jQuery(document).ready(function($) {
	    function setTranslate(xPos, yPos, el) {
	        el.style.transform = "translate3d(" + xPos + "px, " + yPos + "px, 0)";
	    } 
			var $stuck = $('.stuck-image');
			console.log(top);
			var imgHeight = $stuck.height(),
				contentHeight = $('.media-content .title-div').height();
			$('.media-wrap').height(imgHeight + contentHeight);
		    if($(document).width() > 500){
				var top = $stuck.offset().top + contentHeight;
			}
			else{
				$('.stuck-image').css('display','none');
				var top =  contentHeight + 50;
			}
			window.addEventListener("DOMContentLoaded", scrollFade, false);

			console.log($('.stuck-image'));
			$stuck.css('position','fixed');
			$stuck.css('top',top);
			$stuck.css('left',0);
			$stuck.css('z-index',0);
		    if($(document).width() <= 500){
				$('.stuck-image').fadeIn('fast');
			}
	    var yScrollPositionFade;
	 
	    function scrollFade() {
	        var yScrollPositionFade = window.scrollY;
	        var docWidth = $(document).width();
	        var multiplier = 1;
	        if(docWidth < 1200 && docWidth > 1000 ){
	        	multiplier = 1.7;
	        }
	        if(docWidth <= 1000){
	        	multiplier = 2.0;
	        }
	        if(docWidth <= 1000){
	        	multiplier = 2;
	        }
	        if(docWidth <= 700){
	        	multiplier = 1.8;
	        }
	        multiplier = 1;
	        var fadeCoef = (top - contentHeight*multiplier) /window.scrollY;
	        if(fadeCoef > 1){
	        	fadeCoef = 1;
	        }
	        if(fadeCoef < 0.1){
	        	fadeCoef = 0;
	        }
	    	if( $(document).width() <= 500 ){ 
	    		if(window.scrollY > 800){
			        $stuck.css('opacity',0);
			    }
			    else{
			        $stuck.css('opacity',.7);
			    }
	    	}
	    	else{
		        $stuck.css('opacity',fadeCoef);
		    }
	        //setTranslate(0, yScrollPosition * -.3, imageTwo); 
	        requestAnimationFrame(scrollFade);
		    
	    }
	});


</script>
<style>
#html5-watermark{
	display:none !important;
}
</style>