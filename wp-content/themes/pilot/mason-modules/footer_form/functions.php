<?php
	function build_footer_form_layout(){
		$args = array(
			'title' => mason_get_sub_field('footer_form_block_title'),
			'image' => mason_get_sub_field('footer_form_block_image'),
			'content' => mason_get_sub_field('footer_form_block_content'),
		);
		return $args;
	}
?>