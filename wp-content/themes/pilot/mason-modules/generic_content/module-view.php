<?php 
	/**
	 * string	$args['title']
	 * string	$args['content']
	 */
	global $args;
	$card_class = "";
	if( $args['use_card'] ){
		$card_class = "shadow-card";
	}
?>
<div class="<?php echo $card_class; ?> interior-container interior-box" >
	<div class="gc-wrap ">
		<div class="gc-content">
			<?php echo $args['content']; ?>
		</div>
	</div>
</div>