<?php
	function build_generic_content_layout(){
		$args = array(
			'title' => mason_get_sub_field('generic_content_block_title'),
			'use_card' => mason_get_sub_field('generic_content_block_use_card'),
			'content' => mason_get_sub_field('generic_content_block_content'),
		);
		return $args;
	}
?>