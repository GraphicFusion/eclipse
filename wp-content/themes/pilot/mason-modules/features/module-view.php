<?php
	/**
	 * string	$args['rows']
	 * string	$args['rows'][0]['image']
	 * string	$args['rows'][0]['title']
	 * string	$args['rows'][0]['text']
	 * string	$args['rows'][0]['button_text']
	 * string	$args['rows'][0]['button_link']
	 */
	global $args;
?>
<?php if(is_array($args['rows'])): $row_counter = 0; ?>
	<div class='features-section'>
		<div class="features-fade"style="position:relative" >
			<div id="features_anchor" style="position:absolute;bottom:100px; "></div>
			
		</div>
		<div class="feature-wrapper">
			<div class="interior-box" >
				<h3 >KEY FEATURES</h3>
			</div>
			<div class='features-content-section interior-box '>
		<?php foreach( $args['rows'] as $row ) : ?>
						<div class='feature'>
							<div class='innersection'>
							<img class='features-block-image' src='<?php echo $row['image']['url']; ?>'>
								<h4 class="feature-title"><?php echo $row['title']; ?></h4>
								<p><?php echo $row['text']; ?></p>
							</div><!--innersection--->
						</div><!--features-content-section--->
		<?php endforeach; ?>
	  		</div><!--/features-section--->
	  	</div>
	</div>
<?php endif; ?>