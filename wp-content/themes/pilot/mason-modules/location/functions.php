<?php
	function build_location_layout(){
		$args = array(
			'title' => get_sub_field('location_block_title'),
			'address' => mason_get_sub_field('location_block_address'),
			'button' => mason_get_sub_field('location_block_link')
		);
		return $args;
	}
?>