<?php 
	/**
	 * string	$args['title']
	 * string	$args['content']
	 */
	global $args;
?>
<div class="shadow-card interior-container fadeIn animated slower">
	<?php foreach($args['repeater'] as $block) : ?>
		<?php if($block['icon_active_row']) : ?>
			<?php
				$button_href = "";
				$button_text = $block['icon_block_link_text'];
				if(isset($block['icon_block_custom_link'])){
					$button_href = $block['icon_block_custom_link'];
				}
				else{
					$button_href = $block['icon_block_link'];				
				}
				$class = "left-img";
				if($block['icon_image_alignment']){
					$class = "right-img";
				}

				if(isset($block['icon_image'])){
					$image = '<div class="img-wrapper '.$class.'"><img class="content-img" src="'.$block['icon_image']['url'].'"></div>';
				}
			?>

			<div class="icon-wrap  <?php echo $class; ?>">
				<?php echo $image; ?>
				<div class="icon-content ">
					<div class="title ">
						<?php if( isset( $block['icon_subtitle'] ) ) : ?>
							<h6><?php echo $block['icon_subtitle']; ?></h6>
						<?php endif; ?>
						<?php if( isset( $block['icon_title'] ) ) : ?>
							<h4><?php echo $block['icon_title']; ?></h4>
						<?php endif; ?>
						<?php if( isset( $block['icon_content'] ) ) : ?>
							<p><?php echo $block['icon_content']; ?></p>
						<?php endif; ?>
						<?php if($button_href ): ?>
							<a href="<?php echo $button_href; ?>" class="udrive-button orange-fade-horizontal ">
								<h5><?php echo $button_text; ?></h5>
							</a>
						<?php endif; ?>
					</div><!--/title-->
				</div>
			</div>
		<?php endif; ?>
	<?php endforeach; ?>
</div>