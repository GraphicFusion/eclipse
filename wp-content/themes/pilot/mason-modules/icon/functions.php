<?php
	function build_icon_layout(){
		$args = array(
			'title' => get_sub_field('icon_block_title'),
			'repeater' => mason_get_sub_field('icon_block_repeater'),
		);
		return $args;
	}
	function icon_admin_enqueue($hook) {
		wp_register_style( 'media_wp_admin_css', get_template_directory_uri() . '/mason-modules/icon/admin-style.css', false, '1.0.0' );
        wp_enqueue_style( 'media_wp_admin_css' );
    }
	add_action( 'admin_enqueue_scripts', 'icon_admin_enqueue' );

?>