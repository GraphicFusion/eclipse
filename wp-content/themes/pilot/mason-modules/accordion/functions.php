<?php
	function build_accordion_layout(){
		$args = array(
			'title' => mason_get_sub_field('accordion_block_title'),
			'content' => mason_get_sub_field('accordion_block_content'),
			'rows' => mason_get_sub_field('accordion_block_rows')
		);
		return $args;
	}
?>