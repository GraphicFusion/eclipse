<?php
	global $args;
?>
	<?php if($args['negative_margin'] ) : ?>
		<?php
			$margin = "-100px;"; 
			if($args['negative_margin_amt'] ){
				$margin = "-".$args['negative_margin_amt']."px";
			} 
		?>
		<style>
			#<?php echo $args['id']; ?>{
				margin-bottom:<?php echo $margin; ?>;
			}
		</style>
	<?php endif; ?>
	<?php if( isset( $args['image_url'] ) || isset( $args['logo'] ) ) : ?>
	<div class="img-block-wrap pull-up " id="<?php echo $args['id']; ?>">
		<div class="bg-image" style="background-image: url(<?php echo $args['image_url']; ?>);">
			<div class="container content-container interior-container dark-card fadeIn animated slower">
				<div class="title">
					<div class="title-wrap">
						<?php if( isset( $args['left_image'] ) ) : ?>
							<div class="left-image">
								<img src='<?php echo  $args['left_image']; ?>'>
							</div>
						<?php endif; ?>
						<div class="right-content">
							<div class="subtitle"><?php echo $args['subtitle']; ?></div>
							<?php if( isset( $args['title'] ) ) : ?>
								<div class=" media_title">
									<?php echo $args['title']; ?>
								</div><!--/text_effect-->
							<?php endif; ?>
							<?php if( isset( $args['content'] ) ) : ?>
								<div class=" media_content">
									<?php echo $args['content']; ?>
								</div>
							<?php endif; ?>
							<?php if( $args['button']  ) : ?>
								<div class="button-wrapper">
									<a target="<?php echo $args['button']['target']; ?>" href="<?php echo $args['button']['url']; ?>" class="udrive-button orange-fade-horizontal ">
										<h5><?php echo $args['button']['title']; ?></h5>
									</a>
								</div>
							<?php endif; ?>
						</div>
					</div><!--/title-wrap-->
				</div><!--/title-->
        	</div><!--/container--> 			
		<img class="down-arrow" src="/wp-content/themes/pilot/mason-modules/banner/arrow-down.svg">
		</div><!--/bg-img-->
	</div><!--img-block-wrap-->
	<?php endif; ?>
	<script>
		function scrollDown() {
		  var vheight = $(window).height() - 50;
		  $('html, body').animate({
		    scrollTop: (Math.floor( $(window).scrollTop()  / vheight)+1) * vheight
		  }, 500);  
		};
		function scrollMap() {
		  var vheight = $(window).height() - 50;
		  $('html, body').animate({
		    scrollTop: (Math.floor( $(window).scrollTop()  / vheight)+1) * vheight
		  }, 500);  
		};
		$('.down-arrow').click( function(){
				scrollDown();
		} );
		$('.block-banner .udrive-button').click( function(){
			scrollMap();
		} );

	</script>